#! /bin/bash
# MITM-Helper - Helper scripts for analyzing network traffic as a man in the middle
# Copyright (C) 2021 Jonas Lochmann
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
set -euo pipefail

CERT=~/.mitmproxy/mitmproxy-ca-cert.cer

if [ ! -f "$CERT" ]; then
  echo "There is not certificate file yet, please start mitmproxy once" >&2
  exit 1
fi

TARGET_FILENAME="$(openssl x509 -inform PEM -subject_hash_old -in "$CERT" | head -1).0"
TARGET_PATH="/system/etc/security/cacerts/$TARGET_FILENAME"
echo "target filename: $TARGET_FILENAME"

# adb shell mount -o rw,remount /
adb shell rm "$TARGET_PATH"
cat "$CERT" | adb shell tee "$TARGET_PATH"
adb shell chmod 644 "$TARGET_PATH"
adb shell chcon u:object_r:system_security_cacerts_file:s0 "$TARGET_PATH"
adb shell ls -laZ "$TARGET_PATH"

echo Done
