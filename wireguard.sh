#! /bin/bash
# MITM-Helper - Helper scripts for analyzing network traffic as a man in the middle
# Copyright (C) 2021 Jonas Lochmann
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
set -euo pipefail

check_forwarding_disabled() {
  local O1
  local O2
  local SYSCTL_FAILED=0

  O1="$(sysctl -b net.ipv4.ip_forward)" || SYSCTL_FAILED=1
  O2="$(sysctl -b net.ipv6.conf.all.forwarding)" || SYSCTL_FAILED=1

  if [ "$SYSCTL_FAILED" = "1" ]; then
    echo "Could not launch sysctl, are you running this as root?" >&2
    return 1
  fi

  if [ "$O1" = "0" ] && [ "$O2" = "0" ]; then
    return 0
  else
    echo $O2
    echo "Forwarding is already enabled" >&2
    echo "This script could cause trouble with this, so it does nothing" >&2
    return 1
  fi
}

# min max value
check_is_valid_number() {
  if ! [[ $3 =~ ^[0-9]+$ ]] ;then
    return 1
  fi

  if [ $3 -lt $1 ]; then
    return 2
  fi

  if [ $3 -gt $2 ]; then
    return 3
  fi

  return 0
}

# family (inet/inet6)
local_ip_addresses() {
  ip address | awk --assign "family=$1" '{ if ($1 == family) { v = $2; gsub(/\/.*$/, "", v); print v } }'
}

# family (inet/inet6)
select_ip_address() {
  while true; do
    echo "Which $1 IP address of this computer would you like to use to receive and send the traffic?" >&2
    local OPTIONS="$(local_ip_addresses "$1")"
    printf "0\tRefresh this list\n" >&2
    printf "1\tUse no address\n" >&2
    echo "$OPTIONS" | awk 'BEGIN { c = 2 } { print c "\t" $0; c += 1 }' >&2
    local MAX_INDEX="$(echo "$OPTIONS" | awk 'BEGIN { c = 2 } { c += 1 } END { print c - 1 }')"
    local SELECTION; read -e -p "Your selection: " SELECTION; echo "" >&2
    local VALID=1
    check_is_valid_number 0 "$MAX_INDEX" "$SELECTION" || VALID=0
    if [[ "$VALID" = "0" ]]; then
      echo "That's no valid option" >&2
      echo "" >&2
    elif [[ "$SELECTION" != "0" ]]; then
      break
    fi
  done

  echo "$OPTIONS" | awk --assign "s=$SELECTION" 'BEGIN { c = 2 } { if (c == s) print $0; c += 1 }'
}

check_forwarding_disabled
SELF_PUBLIC_IP4="$(select_ip_address inet)"
SELF_PUBLIC_IP6="$(select_ip_address inet6)"

if [ "${SELF_PUBLIC_IP6:-$SELF_PUBLIC_IP4}" = "" ]; then
  echo "You muse chose at least one address to use this"
  exit 1
fi

NETWORK_DEVICE_ID="intercept0"
WIREGUARD_LISTEN_PORT_SELF=6000
WIREGUARD_LISTEN_PORT_REMOTE=6001
NFT_TABLE="intercept_nat"

IP4_NET="10.46.174.0/24"
IP6_NET="fd3c:2b42:6a43::/48"
IP4_SELF="10.46.174.1"
IP4_REMOTE="10.46.174.2"
IP6_SELF="fd3c:2b42:6a43:aad2::"
IP6_REMOTE="fd3c:2b42:6a43:1e9a::"
IP4_SELF_N="${IP4_SELF}/32"
IP4_REMOTE_N="${IP4_REMOTE}/32"
IP6_SELF_N="${IP6_SELF}/64"
IP6_REMOTE_N="${IP6_REMOTE}/64"

PRIVKEY_SELF="$(wg genkey)"
PRIVKEY_REMOTE="$(wg genkey)"
PUBKEY_SELF="$(echo "$PRIVKEY_SELF" | wg pubkey)"
PUBKEY_REMOTE="$(echo "$PRIVKEY_REMOTE" | wg pubkey)"

setup() {
  wg_interface_setup
  nft_setup
  forward_setup
}

CLEANUP_DELETE_WG_INTERFACE=0
CLEANUP_NFT=0
CLEANUP_FORWARD=0
function cleanup() {
  wg_interface_cleanup || true
  nft_cleanup || true
  forward_cleanup || true
}
trap cleanup EXIT

function wg_interface_setup() {
  echo "Setup wireguard interface" >&2
  ip link add dev "$NETWORK_DEVICE_ID" type wireguard
  CLEANUP_DELETE_WG_INTERFACE=1
  echo "$PRIVKEY_SELF" | wg set "$NETWORK_DEVICE_ID" listen-port "$WIREGUARD_LISTEN_PORT_SELF" private-key /dev/stdin
  ip addr add "$IP4_NET" dev "$NETWORK_DEVICE_ID"
  ip addr add "$IP6_NET" dev "$NETWORK_DEVICE_ID"
  wg set "$NETWORK_DEVICE_ID" peer "$PUBKEY_REMOTE" allowed-ips "$IP4_REMOTE_N,$IP6_REMOTE_N"
  ip link set "$NETWORK_DEVICE_ID" up
}

function wg_interface_cleanup() {
  if [ "$CLEANUP_DELETE_WG_INTERFACE" = "1" ]; then
    echo "Shutdown wireguard interface" >&2
    ip link delete dev "$NETWORK_DEVICE_ID"
    CLEANUP_DELETE_WG_INTERFACE=0
  fi
}

function nft_setup() {
  echo "Configure firewall" >&2
  nft add table ip "$NFT_TABLE"
  nft add table ip6 "$NFT_TABLE"
  CLEANUP_NFT=1
  for FAMILY in ip ip6; do
    nft add set "$FAMILY" "$NFT_TABLE" mitmports '{ type inet_service; }'
    nft add chain "$FAMILY" "$NFT_TABLE" forward '{ type filter hook forward priority 0; policy drop ; }' # block by default
    nft add rule "$FAMILY" "$NFT_TABLE" forward iifname intercept0 accept
    nft add rule "$FAMILY" "$NFT_TABLE" forward ct state established,related accept # always allow responses
    #nft add rule "$NFT_TABLE" forward log flags all # log packages were forwarding was blocked
    nft add chain "$FAMILY" "$NFT_TABLE" prerouting '{ type nat hook prerouting priority -100 ; }'
    nft add chain "$FAMILY" "$NFT_TABLE" postrouting '{ type nat hook postrouting priority 100 ; }'
  done

  if [ ! -z "$SELF_PUBLIC_IP4" ]; then
    nft add rule ip "$NFT_TABLE" prerouting ip saddr "$IP4_REMOTE_N" tcp dport @mitmports redirect to :8080
    nft add rule ip "$NFT_TABLE" postrouting ip saddr "$IP4_REMOTE_N" snat "$SELF_PUBLIC_IP4"
  fi

  if [ ! -z "$SELF_PUBLIC_IP6" ]; then
    nft add rule ip6 "$NFT_TABLE" prerouting ip6 saddr "$IP6_REMOTE_N" tcp dport @mitmports redirect to :8080
    nft add rule ip6 "$NFT_TABLE" postrouting ip6 saddr "$IP6_REMOTE_N" snat "$SELF_PUBLIC_IP6"
  fi
}

function nft_cleanup() {
  if [ "$CLEANUP_NFT" = "1" ]; then
    echo "Remove firewall configuration" >&2
    nft delete table ip "$NFT_TABLE"
    nft delete table ip6 "$NFT_TABLE"
    CLEANUP_NFT=0
  fi
}

forward_setup() {
  echo "Enable forwarding" >&2
  CLEANUP_FORWARD=1
  sysctl -w net.ipv4.ip_forward=1 > /dev/null
  sysctl -w net.ipv6.conf.all.forwarding=1 > /dev/null
}

forward_cleanup() {
  if [ "$CLEANUP_FORWARD" = "1" ]; then
    echo "Disable forwarding" >&2
    sysctl -w net.ipv4.ip_forward=0 > /dev/null
    sysctl -w net.ipv6.conf.all.forwarding=0 > /dev/null
  fi
}

setup

generate_client_config() {
  local ENDPOINT=""

  if [ "$SELF_PUBLIC_IP6" != "" ]; then
    ENDPOINT="[$SELF_PUBLIC_IP6]"
  else
    ENDPOINT="$SELF_PUBLIC_IP4"
  fi

  cat  <<EOF
[Interface]
Address = ${IP4_REMOTE_N}, ${IP6_REMOTE_N}
ListenPort = $WIREGUARD_LISTEN_PORT_REMOTE
PrivateKey = $PRIVKEY_REMOTE

[Peer]
PublicKey = $PUBKEY_SELF
AllowedIPs = 0.0.0.0/0, ::/0
Endpoint = ${ENDPOINT}:${WIREGUARD_LISTEN_PORT_SELF}
EOF
}

client_config_wizard() {
  while true; do
    echo "How would you like to do the client setup?">&2
    printf "0\tCancel\n" >&2
    printf "1\tOutput the config file as text\n" >&2
    printf "2\tOutput the config file as barcode\n" >&2
    printf "3\tCheck the connection\n" >&2
    printf "4\tContinue without connected device\n" >&2

    local SELECTION; read -e -p "Your selection: " SELECTION; echo "" >&2
    if [ "$SELECTION" = "0" ]; then
      return 1
    elif [ "$SELECTION" = "1" ]; then
      echo "# BEGIN OF CLIENT CONFIGURATION"
      generate_client_config
      echo "# END OF CLIENT CONFIGURATION"
      echo ""
    elif [ "$SELECTION" = "2" ]; then
      generate_client_config | qrencode -t UTF8 -m 2 || (echo "Sorry, that did not work" >&2)
    elif [ "$SELECTION" = "3" ]; then
      local OK=1
      ping -c1 -w1 "$IP4_REMOTE" || OK=0

      if [ "$OK" = "1" ]; then
        return 0
      fi

      echo "Looks like the device did not connect yet" >&2
      echo "" >&2
    elif [ "$SELECTION" = "4" ]; then
      echo "REMEMBER: You most likely won't see any traffic" >&2
      return 0
    else
      echo "That's no valid selection" >&2
      echo "" >&2
    fi
  done
}

running_wizard() {
  echo "" >&2
  echo "Ready; You can use wireshark now to take a look at the traffic of $NETWORK_DEVICE_ID" >&2
  echo "If you want, you can redirect some TCP ports to your local port 8080 to intercept traffic there" >&2
  echo "" >&2

  while true; do
    echo "What would you like to do now?">&2
    printf "0\tShutdown\n" >&2
    printf "1\tList intercepted ports\n" >&2
    printf "2\tEnable interception for a port\n" >&2
    printf "3\tDisable interception for all ports\n" >&2

    local SELECTION; read -e -p "Your selection: " SELECTION; echo "" >&2
    if [ "$SELECTION" = "0" ]; then
      return 0
    elif [ "$SELECTION" = "1" ]; then
      nft list set ip "$NFT_TABLE" mitmports # this only outputs the ipv4 config, but it should always match v6
    elif [ "$SELECTION" = "2" ]; then
      echo "Checking if something waits for interception ..." >&2
      local OK=1
      nc -zv -w1 ::1 8080 || OK=0

      if [ "$OK" = "0" ]; then
        echo "" >&2
        echo "There is nothing yet listening at port 8080 -> the interception would not work" >&2
        echo "" >&2
        continue
      fi

      local SELECTION; read -e -p "Which port would you like to intercept: " SELECTION; echo "" >&2

      local VALID=1
      check_is_valid_number 1 65535 "$SELECTION" || VALID=0
      if [[ "$VALID" = "0" ]]; then
        echo "That's no valid option" >&2
        echo "" >&2
      else
        nft add element ip "$NFT_TABLE" mitmports "{ $SELECTION }"
        nft add element ip6 "$NFT_TABLE" mitmports "{ $SELECTION }"
      fi
    elif [ "$SELECTION" = "3" ]; then
      nft flush set ip "$NFT_TABLE" mitmports
      nft flush set ip6 "$NFT_TABLE" mitmports
    else
      echo "That's no valid option" >&2
      echo "" >&2
    fi
  done
}

client_config_wizard
running_wizard
