# MITM-Helper

Helper scripts for analyzing network traffic as a man in the middle

## wireguard.sh

This script setups a wireguard server. Everything which is routed over it can
be viewed with tools like wireshark.

### Requirements

- qrencode (optional)
- wireguard-tools
- nftables
- netcat-openbsd
- iproute2
- sysctl (part of systemd)

### Usage

Launch the script with root permissions.

### What does it do?

It setups a wireguard server which redirects the traffic from the client using network
address translation. It creates a configuration for exactly one client. When you've finished
your experiments, it disables the wireguard server again.

It is possible to enable and disable per port if it should be redirected to ``localhost:8080``.
This makes it easy to use ``mitmproxy --mode transparent --showhost`` because the only thing
one must do is enabling the redirection for port 80 and 443.

There is the option to show the client configuration as text or as barcode. The barcode is very
confortable when using the [Wireguard Android Client](https://f-droid.org/en/packages/com.wireguard.android/)
which can scan it to use the configuration.

To make sure that you don't forget to connect the client, this script sends a ``ping`` over the tunnel before it leaves the setup phase. It is possible to skip this, but that is not recommend.

## android-install-mitmproxy-certificate.sh

<https://docs.mitmproxy.org/stable/howto-install-system-trusted-ca-android/> tells one how
to trust the mitmproxy certificate in the Android Emulator. This script does
the same for a real device with an unlocked bootloader and custom recovery (like TWRP).

### Requirements

- openssl
- adb

### Usage

1. launch mitmproxy once so that it generates its certificate
2. launch the custom recovery at the phone
3. mount the system partition in the recovery system and make sure that it is not mounted read only
4. connect the phone to the computer
5. run the script
6. reboot the phone
7. check that the system knows a CA which is called ``mitmproxy``

During running the script, messages that start with ``libc: Access denied finding property`` can be ignored.
They are not related to the executed commands.

### What does it do?

This can be seen at its source code. It reads the certificate, calculates the
required filename, copies it to the device using adb and sets the permissions
and selinux context so that it looks like the other certificates.

## License

This license only applies to the scripts, not to the tools which are used by the scripts.

MITM-Helper - Helper scripts for analyzing network traffic as a man in the middle  
Copyright (C) 2021 Jonas Lochmann

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
